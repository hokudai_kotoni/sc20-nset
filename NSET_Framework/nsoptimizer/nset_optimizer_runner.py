from __future__ import print_function, division
import sys, os
sys.path.append(os.path.abspath("."))
from configs import REPEATS

#import matplotlib as mpl
#import matplotlib.pyplot as plt
#import mpl_toolkits.mplot3d as a3
#from matplotlib.path import Path
#import matplotlib.patches as patches

import json


from utils.stat import Stat


# Problems
from problems.workflow.wf3 import WF3
from problems.workflow_V5.wf5 import WF5

# Optimizers
from algorithms.nset.nset import NSET


__author__ = 'kotoni'

#Paths to the various data sources
#wf_dp = sys.argv[2][:-1]
#csp_dp = sys.argv[3][:-1]
#feasibles_dp = sys.argv[4][:-1]
#metrics_dp = sys.argv[5][:-1]

#Desired number of solutions
#num_solns = sys.argv[6][:-1]


problems = [  
  #WF1(),
  WF3()
]

algorithms = [
  NSET,
  
  #NSGA3,
]


expt_id = sys.argv[1][-1:-1]



for problem in problems:
  print("Optimization problem (Name_Variables_Objectives): ", problem.title())
  for algo in algorithms:
    print("Algorithm name: ", algo.__name__)
    print("\nFinding optimal configurations:")
    opt = algo(problem)
    opt.run()
    opt.stat.to_json(1)
    print()
    
    # Store and process solutions
      
#expt_id = sys.argv[1]

#Stat.plot_experiment(expt_id)

os.system('python optimizer_final_results.py')