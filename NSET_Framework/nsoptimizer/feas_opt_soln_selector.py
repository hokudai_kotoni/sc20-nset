#Script to select one solution from the feasible optimal universe

import json



__author__ = 'kotoni'


with open('output_data/feas_optimal_configs_data.json', 'r') as f:
     all_feas_solns = json.load(f)


first_feas_soln = all_feas_solns['job_name'][0]


with open('output_data/plan.json', 'w') as f:
     json.dump(first_feas_soln, f, indent=4)



def reconf_job(invocation_id, tool_id):
	
	out_to_reconf = {}
	
	out_to_reconf[invocation_id] = []
	
	instance = 'null'
	
	with open('output_data/plan.json', 'r') as soln_dat:
		single_soln = json.load(soln_dat)
	
	for tool in single_soln['structure']:
		if tool['tool_name'] == tool_id:
			instance = tool['instance_type']
	
	out_to_reconf[invocation_id].append({
   "variables": ["i1"], "constraints": [
       ["=", ["flavor", "i1"], instance]
   ]})
	
	return out_to_reconf[invocation_id]
	
		

if __name__ == "__main__":
	galaxy_invocation_id = 7007
	tool_id = "stringtie"
	
	testing = reconf_job(galaxy_invocation_id, tool_id)
	print(testing)

