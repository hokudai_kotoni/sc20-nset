## This explains how to startup and run the optimization process from a notebook

### Clone the code from the Repository

```
git clone https://powellcourtney@bitbucket.org/hokudaicrestgroup/nonevoloptimizer.git
```

### Create the docker container in which to run the optimizer
#### Go to the folder containing the dockerfile for the image
```
cd nonevoloptimizer/containerz/opt_container
```

#### Build the optimizer Docker image (image name  = nonevol)
```
docker build -t nonevol .
```

#### Backtrack to the parent folder (i.e. nonevoloptimizer)
```
cd ..
cd ..
```

#### Execute the optimizer container
##### (This mounts all of the nonevoloptimizer folder into the container as the working directory)
```
docker run -p 8888:8888 -p 3000:3000 -it -v $(pwd):/home/jovyan nonevol
```
##### Should get a URL similar to the one below

(http://(b86afc86ce69 or 127.0.0.1):8888/?token=7ea2ecc89fd4bbd31d6b89f604354172258409c86bd2ca8a)

##### Delete "(b86afc86ce69 or" and ")", then press enter
##### This brings up the Jupyter interface, from which you can access the notebook