"""
This file contains configuration of the
problems and optimizers
"""
from __future__ import print_function, division
from utils.lib import O

__author__ = 'panzer'

#GENS = 600
#SSEVALS = 100
POP = 50
GENS = 100
REPEATS = 0


def nset_settings():
  """
  Default Settings for NSET
  """
  return O(
    pop_size = POP,    # Size of Population
    gens = GENS,      # Number of generations
  )



def gale_settings():
  """
  Default Settings for GALE
  """
  return O(
    pop_size        = 100,    # Size of Population
    gens            = GENS,   # Number of generations
    allowDomination = True,   # Domination Flag
    gamma           = 0.15    # Gamma factor for mutation
  )

def nsga2_settings():
  """
  Default Settings for NSGA2
  """
  return O(
    pop_size = 100,  # Size of population
    gens = GENS      # Number of generations
  )

def nsga3_settings():
  """
  Default Settings for NSGA3
  """
  return O(
    pop_size = POP,    # Size of Population
    gens = GENS,      # Number of generations
    cr = 1,           # Crossover rate for SBX
    nc = 30,          # eta for SBX (simulated binary crossover between mom & dad)
    nm = 20           # eta (pop size used for distribution) for Mutation
  )

def ss_nsga3_settings():
  """
  Default Settings for SS_NSGA3
  """
  return O(
    pop_size = POP,    # Size of Population
    #gens = GENS,      # Number of generations
    gens = SSEVALS,      # Number of generations
    cr = 1,           # Crossover rate for SBX
    nc = 30,          # eta for SBX
    nm = 20           # eta for Mutation
  )


def ss_nsga3_rndkid_settings():
  """
  Default Settings for steady state NSGA3
  implemented using one random offspring.
  """
  return O(
    pop_size = POP,    # Size of Population
    #gens = GENS,      # Number of generations
    gens = SSEVALS,      # Number of generations
    cr = 1,           # Crossover rate for SBX
    nc = 30,          # eta for SBX (simulated binary crossover between mom & dad)
    nm = 20           # eta (pop size used for distribution) for Mutation
  )

def ss_nsga3_elitekid_settings():
  """
  Default Settings for steady state NSGA3
  implemented using one offspring from the first rank.
  """
  return O(
    pop_size = POP,    # Size of Population
    #gens = GENS,      # Number of generations
    gens = SSEVALS,      # Number of generations
    cr = 1,           # Crossover rate for SBX
    nc = 30,          # eta for SBX (simulated binary crossover between mom & dad)
    nm = 20           # eta (pop size used for distribution) for Mutation
  )


def ss_nsga3_elitepar_settings():
  """
  Default Settings for steady state NSGA3
  implemented using two parents from the first rank 
  to produce one offspring.
  """
  return O(
    pop_size = POP,    # Size of Population
    #gens = GENS,      # Number of generations
    gens = SSEVALS,      # Number of generations
    cr = 1,           # Crossover rate for SBX
    nc = 30,          # eta for SBX (simulated binary crossover between mom & dad)
    nm = 20           # eta (pop size used for distribution) for Mutation
  )

def de_settings():
  """
  Default Settings for DE
  """
  return O(
    pop_size = 50,    # Size of Population
    gens = GENS,      # Number of generations
    f = 0.75,         # Mutation Factor
    cr = 0.3          # Crossover Rate
  )

def moead_settings():
  """
  Default MOEA/D settings
  """
  return O(
    pop_size = 91,      # Size of Population
    gens = GENS,        # Number of generations
    distance = "pbi",   # Distance metric
    T = 20,             # Closest weight vectors.
    penalty = 5,        # Penalty parameter for PBI distance
    crossover = "sbx",   # Crossover Metric
    cr = 1,             # Crossover rate for SBX
    nc = 20,            # eta for SBX
    nm = 20,            # eta for Mutation
    de_np = 0.9,        # DE neighborhood probability
    de_cr = 0.5         # DE crossover rate
  )
