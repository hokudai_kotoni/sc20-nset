from __future__ import print_function, division
import sys, os, math
sys.path.append(os.path.abspath("."))
from os import path
from problems.problem import *

__author__ = 'kotoni'

import json
import copy
import itertools

import time

def looV2(points):
  """
  Iterator which generates a
  test case and training set
  :param points:
  :return:
  """
  for i in range(len(points)):
    elem = points[0]
    rem = points[1:] #points[:i] + points[i+1:]
    points.pop(0)
    yield elem, rem


class WF3(Problem):
    """
    6 decisions and 3 objectives
    """
    dirpath = os.getcwd()
    print(dirpath)
    def __init__(self, n=6):
    	self.wf_data = json.loads(open("problems/workflow/workflow_structure.json").read())
    	self.csp_data = json.loads(open("problems/workflow/csp_bigdata_db_2018.json").read())
    	self.metrics_data = json.loads(open("problems/workflow/workflow_metrics_db.json").read())
    	
    	# Access feasible configurations file in constraint solver folder
    	os.chdir("..")
    	self.feasible_configs = json.loads(open("etconstraintsolver/data/feasible_answer.json").read())
    	
    	# Return to optimizer top level directory
    	os.chdir("nsoptimizer")
    	
    	self.wf_struct = self.workflowDataToListFormat()
    	#print(csp_dp)
        """
        Initialize the problem
        :param n: Number of decisions
        :return:
        """
        Problem.__init__(self)
        self.name = WF3.__name__
        self.decisions = [Decision("x"+str(index+1),1,14) for index in range(n)]
        
        self.seed_pop = [[3, 2, 1, 9, 7, 7], [3, 2, 1, 9, 7, 9], [3, 2, 1, 9, 9, 7], [3, 2, 1, 9, 9, 9], [4, 2, 1, 7, 7, 7], [4, 2, 1, 7, 7, 9]]
        
        #self.seed_pop = [[3, 2, 1, 9, 7, 7], [3, 2, 1, 9, 7, 9], [3, 2, 1, 9, 9, 7], [3, 2, 1, 9, 9, 9], [4, 2, 1, 7, 7, 7], [4, 2, 1, 7, 7, 9], [4, 2, 1, 7, 9, 7], [4, 2, 1, 7, 9, 9], [4, 2, 1, 9, 7, 7], [4, 2, 1, 9, 7, 9], [4, 2, 1, 9, 9, 7], [4, 2, 1, 9, 9, 9], [5, 2, 1, 7, 7, 7], [5, 2, 1, 7, 7, 9], [5, 2, 1, 7, 9, 7], [5, 2, 1, 7, 9, 9], [5, 2, 1, 9, 7, 7], [5, 2, 1, 9, 7, 9], [5, 2, 1, 9, 9, 7], [5, 2, 1, 9, 9, 9], [7, 2, 1, 7, 7, 9]]
        

        #print("\n self.decisions = ", self.decisions)
        self.objectives = [Objective("Running Cost", True, 0, 1000), Objective("Makespan", True, 0, 500000), Objective("Availability", False, 0, 1), Objective("Reliability", False, 0, 1)]
    
    def split_list(self, the_list, chunk_size):
        result_list = []
        while the_list:
            result_list.append(the_list[:chunk_size])
            the_list = the_list[chunk_size:]
        return result_list

    def availabilityDataCreator(self, WFTaskList, cspData):
        wf_seq_avail_list = []
        wf_avail = 1
        
        for i in range(len(WFTaskList[0])):
            for region in cspData['aws']['regionID']:
                if region['id'] == WFTaskList[0][i]:
                    region_name = region['regionName']
                    region_prefix = region_name[:2]
        
            for availability in cspData['aws']['availability']:
                if availability['availabilityRegion'] == region_prefix:
                    dc_avalability = availability['availabilityFactor']
                    wf_seq_avail_list.append(dc_avalability)
            
        for dc_avail in wf_seq_avail_list:
            wf_avail = wf_avail * dc_avail
        
        return wf_avail

    def costDataCreator(self, WFTaskList, cspData, metricsData):
        
        costList = [0 for _ in range(len(WFTaskList[0]))]
        
        #print("costList", costList)
        
        server_type_list = [0 for _ in range(len(WFTaskList[0]))]
        
        task_count = 0
    
        #Get task cost using 1st row as guide
        for WFTask in WFTaskList[0]:
            
            #Find instance type
            for instance in cspData['aws']['instanceID']:
                if instance['nodeType'] == 'cmpt' and instance['id'] == WFTaskList[0][task_count]:
                    server_type_list[task_count] = instance['instanceType']
            
            #Find region and cost
            for region in cspData['aws']['regionID']:
                if region['id'] == WFTaskList[1][task_count]:
                    for cost in cspData['aws']['instanceCost']:
                        if (cost['nodeType'] == "cmpt" and cost['instanceType'] == server_type_list[task_count]  and region['regionName'] == cost['region']):
                            costList[task_count] = cost['cost']
            
            task_count += 1
        
        return costList

    
    def singleLevelRunCost(self, runCostList, WFStruct, WFTaskList, cspData, metricsData):
        
        # runCostList has the format [VM0_Cost, VM1_Cost, ...]
        
        WFTask_makespan_list = WFStruct[:]
        task_count = 0
        
        for tool in WFStruct:
            tool_name = tool[0]
            datasize = tool[1][0]
            #num_inputs = tool[1][0]
            
            vm_id = WFTaskList[0][task_count]
            
            singlevm_makespan = self.makespanSingleVM(datasize, tool_name, vm_id, cspData, metricsData)
            
            WFTask_makespan_list[task_count]= singlevm_makespan
            
            task_count += 1
        
        
        runCost = 0
        vm_makespan_cost_list = []
        
        
        for index_1, vm_makespan in enumerate(WFTask_makespan_list):
            vmRunCost  = runCostList[index_1]
            vm_makespan_cost = vmRunCost * (vm_makespan/3600)
            vm_makespan_cost_list.append(vm_makespan_cost)
        
        for vm_actual_cost in vm_makespan_cost_list:
            runCost += vm_actual_cost
        
        return runCost

    
    def runningCost(self, runCostList, WFStruct_Leveled, WFTaskList, cspData, metricsData):
        
        # runCostList has the format [VM0_Cost, VM1_Cost, ...]
        
        total_runCost = 0
        
        for wf_level in WFStruct_Leveled:          
            single_level_cost = self.singleLevelRunCost(runCostList, wf_level, WFTaskList, cspData, metricsData)
            
            total_runCost += single_level_cost
            
            print("single_level_cost: ",single_level_cost)
        
        return total_runCost    
    
    
    
    def makespanSingleVM(self, current_datasize, tool_name, vm_id, cspData, metricsData):       
        for instance in cspData['aws']['instanceID']:
            if instance['nodeType'] == 'cmpt' and instance['id'] == vm_id:
                vm_type = instance['instanceType']        
        
        for metricsSet in metricsData[tool_name][vm_type]:
        	if metricsSet["metricsSetID"] == 1:
        		tool_time = metricsSet['runTime']
        		tool_datasize = metricsSet['inputFileSize']
        	
        
        tool_datasize_gig = tool_datasize/(10^9)
        


        
        makespan_singlevm = tool_time * (current_datasize/tool_datasize_gig)
        
        return makespan_singlevm
        

    def workflowMakespanSingle(self, WFStruct, WFTaskList, cspData, metricsData):
        wf_makespan_list = []
        makespan = 0
        
        segmented_levels_makespan_list = WFStruct[:]	#Copies the list WFStruct
        
        j = 0
        
        
        for level in WFStruct:
            segmented_levels_makespan_list[j] = []
            i = 0
            for tool in level:
                tool_name = tool[0]
                datasize = tool[1][0]
                #num_inputs = tool[1][0]
                
                vm_id = WFTaskList[0][i]
                
                singlevm_makespan = self.makespanSingleVM(datasize, tool_name, vm_id, cspData, metricsData)
                
                segmented_levels_makespan_list[j].append(singlevm_makespan)
            
            wf_makespan_list.append(max(segmented_levels_makespan_list[j]))
            
            i += 1
            j += 1 
        
        for level_makespan in wf_makespan_list:
            makespan += level_makespan

        return makespan	#This gives the makespan in seconds


    def workflowReliability(self, WFStruct, WFTaskList, cspData, metricsData):
        wf_reliability = 1
        reliability = 1
        
        segmented_levels_reliability = 1
        
        j = 0
        
        for level in WFStruct:
            i = 0
            for tool in level:
                tool_name = tool[0]
                datasize = tool[1][0]
                #num_inputs = tool[1][0]
                
                vm_id = WFTaskList[0][i]
                
                
                singlevm_makespan = self.makespanSingleVM(datasize, tool_name, vm_id, cspData, metricsData)
                for instance in cspData['aws']['instanceID']:
                	if instance['nodeType'] == 'cmpt' and instance['id'] == vm_id:
                		vm_type = instance['instanceType']
                
                for vmRel in cspData['aws']['instanceError']:
                	if vmRel['instanceType'] == vm_type:
                		vm_error = vmRel['errorRate']
                
                singlevm_reliability = math.exp(-1 * vm_error * singlevm_makespan)
                
                segmented_levels_reliability = segmented_levels_reliability * singlevm_reliability
            
            wf_reliability = wf_reliability * segmented_levels_reliability
            
            i += 1
            j += 1 
        
        
        #print("\n\n Makespan is :", makespan)
        return wf_reliability	       


    def getFeasSolnsUniv(self):
        feasible_solutions_list = []
        feasible_solutions_universe = []
        feasibleSolutions = self.feasible_configs
        cspData = self.csp_data

        for feasibleSolution in feasibleSolutions['answer']:
            feas_soln_res_format = []
            for tool_count in range(0, 3):
                tool = feasibleSolution['structure'][tool_count]
                tool_name = tool['tool_name']
                tool_vmtype = tool['instance_type']
                tool_vmregion = tool['location']
                feas_soln_res_format.append([tool_name, tool_vmtype, tool_vmregion])
            feasible_solutions_universe.append(feas_soln_res_format)

        feas_solns_list =  []

        for feas_soln in feasible_solutions_universe:
            feas_soln_int = [0 for _ in range(6)]
            for task_count in range(0, 3):
                for instanceType in cspData['aws']['instanceID']:
                    if (instanceType['instanceType'] == feas_soln[task_count][1]):
                        feas_soln_int[task_count] = instanceType['id']
                        for region in cspData['aws']['regionID']:
                            if (region['regionName'] == feas_soln[task_count][2]):
                                feas_soln_int[task_count + 3] = region['id']
            feas_solns_list.append(feas_soln_int)

        feas_univ = feas_solns_list[:]

        return feas_univ
    
    def getMoreSolns(self, feas_univ_, num_offspring):
        feas_univ_2 = feas_univ_[:]
        next_soln_set = []
        next_soln_set_deap = []

        if len(feas_univ_2) == 0:
            next_soln_set = []
        elif len(feas_univ_2) <= num_offspring:
            next_soln_set = feas_univ_2[:]
            feas_univ_2 = []
        else:
            for index in reversed(range(0, num_offspring)):
                next_soln_set.append(feas_univ_2.pop(index))

        return next_soln_set, feas_univ_2


    def getFeasSolnsUniv_Unique(self):
        feasible_solutions_list = []
        feasible_solutions_universe = []
        feasibleSolutions = self.feasible_configs
        cspData = self.csp_data

        for feasibleSolution in feasibleSolutions['answer']:
            feas_soln_res_format = []
            for tool_count in range(0, 3):
                tool = feasibleSolution['structure'][tool_count]
                tool_name = tool['tool_name']
                tool_vmtype = tool['instance_type']
                tool_vmregion = tool['location']
                feas_soln_res_format.append([tool_name, tool_vmtype, tool_vmregion])
            feasible_solutions_universe.append(feas_soln_res_format)
        
        
        feasible_solutions_universe_unique = []
        feasible_solutions_universe_unique_sorted = []
        feas_solns_list_unique =  []
        
        for item in feasible_solutions_universe:
        	if sorted(item) not in feasible_solutions_universe_unique_sorted:
        		feasible_solutions_universe_unique_sorted.append(sorted(item))
        		feasible_solutions_universe_unique.append(item)

        for feas_soln in feasible_solutions_universe_unique:
            feas_soln_int = [0 for _ in range(6)]
            for task_count in range(0, 3):
                for instanceType in cspData['aws']['instanceID']:
                    if (instanceType['instanceType'] == feas_soln[task_count][1]):
                        feas_soln_int[task_count] = instanceType['id']
                        for region in cspData['aws']['regionID']:
                            if (region['regionName'] == feas_soln[task_count][2]):
                                feas_soln_int[task_count + 3] = region['id']
            feas_solns_list_unique.append(feas_soln_int)        
        
        
        feas_univ_unique = feas_solns_list_unique[:]

        return feas_univ_unique



    
    def split_list(self, the_list, chunk_size):
        result_list = []
        while the_list:
            result_list.append(the_list[:chunk_size])
            the_list = the_list[chunk_size:]
        
        return result_list
    
    
    def chromosomeToResource(self, chromosome):
        
        #Round off floating values to integers
        integ_chrom = chromosome[:]
        for index, num in enumerate(chromosome):
        	integ_chrom[index] = int(round(num))        
        
        
        WFStruct = [['tophat2', [2.0]], ['cufflinks', [2.0]], ['stringtie', [2.0]]] 
        
        vmTypeRegionSplit = self.split_list(integ_chrom, 3)
        cspData = self.csp_data

        
        wf_chromToRes_list = []
        
        task_count = 0
        
        for tool in WFStruct:
            tool_name = tool[0]
            vm_id = vmTypeRegionSplit[0][task_count]
            region_id = vmTypeRegionSplit[1][task_count]
            
            for instance in cspData['aws']['instanceID']:
                if instance['nodeType'] == 'cmpt' and instance['id'] == vm_id:
                    vm_type = instance['instanceType']
                    
            for region in cspData['aws']['regionID']:
                if region['id'] == region_id:
                    vm_region = region['regionName']
                    vm_location = region['locationName']
                    
            tool_chromo_to_resource = [tool_name, vm_type, vm_region]
            
            wf_chromToRes_list.append(tool_chromo_to_resource)
            
            task_count += 1
            
        return wf_chromToRes_list


    
    
    
    def removeDuplicates(self, pop):
        no_twins = []
        obj_values = []
        
        for indiv in pop:
        	if (indiv.objectives[0],indiv.objectives[1]) not in obj_values:
        		obj_values.append((indiv.objectives[0],indiv.objectives[1]))
        		no_twins.append(indiv)
        
        return no_twins


    def removeDuplicates_basedon_NormOBjs(self, pop):
        no_twins = []
        normobj_refid_values = []
        
        for indiv in pop:
        	if (indiv.norm_objectives, indiv.reference_id) not in normobj_refid_values:
        		normobj_refid_values.append((indiv.norm_objectives, indiv.reference_id))
        		no_twins.append(indiv)
        
        return no_twins
        
    def removeDuplicates_basedon_RefID(self, pop):
     	no_twins = []
        refid_values = []
        
        for indiv in pop:
        	if indiv.reference_id not in refid_values:
        		refid_values.append(indiv.reference_id)
        		no_twins.append(indiv)
        
        return no_twins


    def removeDuplicates_basedon_RefID_perp_rank(self, pop):
     	#no_twins = []
     	attribs_list = []
     	
     	no_twins = copy.deepcopy(pop)
     	pop_rem = copy.deepcopy(pop)
     	
     	for ind_one in pop:
     		pop_rem.remove(ind_one)
     		for ind_two in pop_rem:
     			if (ind_one in no_twins) and (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank <= ind_one.rank):
     				no_twins.remove(ind_one) 
     				#break
     			elif (ind_two in no_twins) and (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank > ind_one.rank):
     				no_twins.remove(ind_two)
     				#break
     			elif (ind_two.reference_id == 4): 
     				print("\nFound one Ref 4 as ind_two: ", (ind_two.reference_id, ind_two.rank))
     				print(ind_two)
     		if (ind_one.reference_id == 4): 
     			print("\nFound one Ref 4 as ind_one: ", (ind_one.reference_id, ind_one.rank))
     			print(ind_one)

        
        print("\nChecking attributes Ref_ID, Rank...")
        for chk_ind in no_twins:
        	print("Attributes: ", (chk_ind.reference_id, chk_ind.rank))
        return no_twins
    
    
    def normalizeFitnessValues(self, fitness_values):
		#fitness_values is a list of the form [runCost(in $), makespan(in secs), availability]
		# runCost range: 0-800 --> divide by 10
		# makespan range: 0-500,000 --> divide by 500,000
		# availability range: 0.9984-0.9996 --> minus 0.9980 then multiply by 100
		
		runCost_norm = fitness_values[0] / 10
		makespan_norm = fitness_values[1] / 500000
		availability_norm = (fitness_values[2] - 0.9980) * 0.5 * 1000
		#availability_norm = fitness_values[2]
		
		fitness_normalized = [runCost_norm, makespan_norm, availability_norm]
		
		return fitness_normalized


    def workflowDataToListFormat(self):
    	##Get workflow structure data and put it in list format
    	workflow_data = self.wf_data
    	
    	WFStruct_Leveled = []
    	
    	for workflow_struct in workflow_data['workflow']:
    		workflow_struct_len = len(workflow_struct['structure'])
    		for level_num in range(workflow_struct_len+1): 
    			globals()["level" + str(level_num)] = []
    	for tool in workflow_struct['structure']:
    		tool_name = tool['tool_name']
    		data_size = tool['data_size']
    		for tool_level in range(workflow_struct_len+1):
    			if (tool['level'] == tool_level): 
    				globals()["level" + str(tool_level)].append([tool_name, [data_size]])
    	
    	for WFStruct_Levels in range(1, workflow_struct_len+1):
    		WFStruct_Leveled.append(globals()["level" + str(WFStruct_Levels)])
    	
    	
    	return WFStruct_Leveled



    def evaluate(self, chromosome):
        
        #Round off floating values to integers
        integ_chrom = chromosome[:]
        for index, num in enumerate(chromosome):
        	integ_chrom[index] = int(round(num))
        
        WFTaskList = self.split_list(integ_chrom, 3)

        
        
        WFStruct = [['tophat2', [2.0]], ['cufflinks', [2.0]], ['stringtie', [2.0]]]
        
        WFStruct_Leveled = self.wf_struct
        
        cspData = self.csp_data
        metricsData = self.metrics_data
        
        runningCostData = self.costDataCreator(WFTaskList, cspData, metricsData) 
        runCost = self.singleLevelRunCost(runningCostData, WFStruct, WFTaskList, cspData, metricsData)

        
        start_makespan = time.time()
        makespan = self.workflowMakespanSingle(WFStruct_Leveled, WFTaskList, cspData, metricsData)
        end_makespan = time.time()
        time_taken = end_makespan - start_makespan)
        
        systAvailability = self.availabilityDataCreator(WFTaskList, cspData)
        
        systreliability = self.workflowReliability(WFStruct_Leveled, WFTaskList, cspData, metricsData)
        
        min_systAvailability = -1 * systAvailability
        
        round_runCost = round(runCost, 2)
        round_makespan = round(makespan, 2)
        round_systAvailability = round(systAvailability, 3)
        
        f = [runCost, makespan, systAvailability, systreliability]

        
        return f


if __name__ == "__main__":
    print("Startup")
    chromosome = [5.2, 2.1, 0.6, 8.9, 7.0, 2.7]
    wf3 = WF3()
    testing = wf3.evaluate(chromosome)
    testing2 = wf3.split_list(chromosome, 3)
    print("Testing : ", testing)
