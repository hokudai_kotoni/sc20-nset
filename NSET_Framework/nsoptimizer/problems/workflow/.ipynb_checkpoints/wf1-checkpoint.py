from __future__ import print_function, division
import sys, os
sys.path.append(os.path.abspath("."))
from problems.problem import *

__author__ = 'kotoni'

import json
import copy
import itertools

def looV2(points):
  """
  Iterator which generates a
  test case and training set
  :param points:
  :return:
  """
  for i in range(len(points)):
    elem = points[0]
    rem = points[1:] #points[:i] + points[i+1:]
    points.pop(0)
    yield elem, rem


class WF1(Problem):
    """
    8 decisions and 3 objectives
    """
    def __init__(self, n=8):
        """
        Initialize the problem
        :param n: Number of decisions
        :return:
        """
        Problem.__init__(self)
        self.name = WF1.__name__
        self.decisions = [Decision("x"+str(index+1),1,11) for index in range(8)]
        
        self.seed_pop = [[1, 1, 1, 1, 8, 8, 8, 8], [1, 1, 1, 2, 8, 8, 8, 8], [1, 1, 1, 3, 8, 8, 8, 8], [1, 1, 1, 4, 8, 8, 8, 8], [1, 1, 2, 1, 8, 8, 8, 8], [1, 1, 2, 2, 8, 8, 8, 8], [1, 1, 2, 3, 8, 8, 8, 8], [1, 1, 3, 1, 8, 8, 8, 8], [1, 1, 3, 2, 8, 8, 8, 8], [1, 1, 3, 3, 8, 8, 8, 8], [1, 1, 4, 1, 8, 8, 8, 8], [1, 2, 1, 1, 8, 8, 8, 8]]
        
        #self.seed_pop = [[1, 1, 1, 1, 8, 8, 8, 8], [1, 1, 1, 2, 8, 8, 8, 8], [1, 1, 1, 3, 8, 8, 8, 8], [1, 1, 1, 4, 8, 8, 8, 8], [1, 1, 2, 1, 8, 8, 8, 8], [1, 1, 2, 2, 8, 8, 8, 8]]
        #self.seed_pop = [[1, 1, 3, 3, 8, 8, 8, 8], [1, 1, 4, 1, 8, 8, 8, 8], [1, 2, 1, 1, 8, 8, 8, 8], [1, 2, 1, 2, 8, 8, 8, 8], [1, 2, 1, 3, 8, 8, 8, 8], [1, 2, 2, 1, 8, 8, 8, 8]]
        #print("\n self.decisions = ", self.decisions)
        self.objectives = [Objective("Running Cost", True, 0, 1000), Objective("Makespan", True, 0, 500000), Objective("Availability", False, 0, 1)]
    
    def split_list(self, the_list, chunk_size):
        result_list = []
        while the_list:
            result_list.append(the_list[:chunk_size])
            the_list = the_list[chunk_size:]
        return result_list

    def availabilityDataCreator(self, WFTaskList, cspData):
        wf_seq_avail_list = []
        wf_avail = 1
        
        for i in range(len(WFTaskList[0])):
            for region in cspData['aws']['regionID']:
                if region['id'] == WFTaskList[0][i]:
                    region_name = region['regionName']
                    region_prefix = region_name[:2]
        
            for availability in cspData['aws']['availability']:
                if availability['availabilityRegion'] == region_prefix:
                    dc_avalability = availability['availabilityFactor']
                    wf_seq_avail_list.append(dc_avalability)
            
        for dc_avail in wf_seq_avail_list:
            wf_avail = wf_avail * dc_avail
        
        return wf_avail

    def costDataCreator(self, WFTaskList, cspData):
        
        costList = [0 for _ in range(len(WFTaskList[0]))]
        
        #print("costList", costList)
        
        server_type_list = [0 for _ in range(len(WFTaskList[0]))]
        
        task_count = 0
    
        #Get task cost using 1st row as guide
        for WFTask in WFTaskList[0]:
            
            #Find instance type
            for instance in cspData['aws']['instanceID']:
                if instance['nodeType'] == 'cmpt' and instance['id'] == WFTaskList[0][task_count]:
                    server_type_list[task_count] = instance['instanceType']
            
            #Find region and cost
            for region in cspData['aws']['regionID']:
                if region['id'] == WFTaskList[1][task_count]:
                    for cost in cspData['aws']['instanceCost']:
                        if (cost['nodeType'] == "cmpt" and cost['instanceType'] == server_type_list[task_count]  and region['regionName'] == cost['region']):
                            costList[task_count] = cost['cost']
            
            task_count += 1
        
        return costList

    def runningCost(self, runCostList, WFStruct, WFTaskList, cspData):
        
        # runCostList has the format [VM0_Cost, VM1_Cost, ...]
        
        WFTask_makespan_list = WFStruct[:]
        task_count = 0
        
        for tool in WFStruct:
            tool_name = tool[0]
            datasize = tool[1][1]
            num_inputs = tool[1][0]
            
            vm_id = WFTaskList[0][task_count]
            
            singlevm_makespan = self.makespanSingleVM(datasize, tool_name, num_inputs, vm_id, cspData)
            
            WFTask_makespan_list[task_count]= singlevm_makespan
            
            task_count += 1
        
        
        runCost = 0
        vm_makespan_cost_list = []
        
        
        for index_1, vm_makespan in enumerate(WFTask_makespan_list):
            vmRunCost  = runCostList[index_1]
            vm_makespan_cost = vmRunCost * (vm_makespan/3600)
            vm_makespan_cost_list.append(vm_makespan_cost)
        
        for vm_actual_cost in vm_makespan_cost_list:
            runCost += vm_actual_cost
        
        return runCost

    def makespanSingleVM(self, current_datasize, tool_name, num_inputs, vm_id, cspData):
        for tool in cspData['workflow']['baseData']:
            if tool['toolName'] == tool_name and tool['inputs'] == num_inputs:
                tool_time = tool['time']
                tool_datasize = tool['dataSize']
                tool_score = tool['score']
        
        for instance in cspData['aws']['instanceID']:
            if instance['nodeType'] == 'cmpt' and instance['id'] == vm_id:
                vm_type = instance['instanceType']
        
        for performance in cspData['aws']['instancePerformance']:
            if performance['instanceType'] == vm_type:
                vm_score = performance['score']
        
        makespan_singlevm = (tool_time * (current_datasize/tool_datasize)) / (tool_score/vm_score)
        
        return makespan_singlevm
        

    def workflowMakespanSingle(self, WFStruct, WFTaskList, cspData):
        wf_makespan_list = []
        makespan = 0
        
        segmented_levels_makespan_list = WFStruct[:]	#Copies the list WFStruct
        
        j = 0
        
        for level in WFStruct:
            segmented_levels_makespan_list[j] = []
            i = 0
            for tool in level:
                tool_name = tool[0]
                datasize = tool[1][1]
                num_inputs = tool[1][0]
                
                vm_id = WFTaskList[0][i]
                
                singlevm_makespan = self.makespanSingleVM(datasize, tool_name, num_inputs, vm_id, cspData)
                
                segmented_levels_makespan_list[j].append(singlevm_makespan)
            
            wf_makespan_list.append(max(segmented_levels_makespan_list[j]))
            
            i += 1
            j += 1 
        
        for level_makespan in wf_makespan_list:
            makespan += level_makespan
        
        #print("\n\n Makespan is :", makespan)
        return makespan/3600	#This gives the makespan in hours


    def getFeasSolnsUniv(self):
        feasible_solutions_list = []
        feasible_solutions_universe = []
        feasibleSolutions = json.loads(open("problems/workflow/answer_sidney_tokyo.json").read())
        cspData = json.loads(open("problems/workflow/csp_bigdata_db.json").read())

        for feasibleSolution in feasibleSolutions['answer']:
            feas_soln_res_format = []
            for tool_count in range(0, 4):
                tool = feasibleSolution['structure'][0]['compute'][tool_count]
                #print("index: ", tool_count)
                #print("toolname: ",tool['time'][0])
                #tool_1 = feasibleSolution['structure'][0]['compute'][0]
                tool_name = tool['time'][0]
                tool_vmtype = tool['instance_type']
                tool_vmregion = tool['location']
                feas_soln_res_format.append([tool_name, tool_vmtype, tool_vmregion])

            #print("\nfeas_soln_res_format: ", feas_soln_res_format)
            feasible_solutions_universe.append(feas_soln_res_format)

        feas_solns_list =  []

        for feas_soln in feasible_solutions_universe:
            feas_soln_int = [0 for _ in range(8)]
            for task_count in range(0, 4):
                for instanceType in cspData['aws']['instanceID']:
                    #print("\n uniq_feas_soln[task_count][1]: ", uniq_feas_soln[task_count][1])
                    if (instanceType['instanceType'] == feas_soln[task_count][1]):
                        #print("\n uniq_feas_soln: ", uniq_feas_soln)
                        #print("\n uniq_feas_soln[task_count][1]: ", uniq_feas_soln[task_count][1])
                        feas_soln_int[task_count] = instanceType['id']
                        for region in cspData['aws']['regionID']:
                            if (region['regionName'] == feas_soln[task_count][2]):
                                #print("\n uniq_feas_soln[task_count][2]: ", uniq_feas_soln[task_count][2])
                                feas_soln_int[task_count + 4] = region['id']
            feas_solns_list.append(feas_soln_int)

        feas_univ = feas_solns_list[:]

        return feas_univ
    
    def getMoreSolns(self, feas_univ_, num_offspring):
        feas_univ_2 = feas_univ_[:]
        next_soln_set = []
        next_soln_set_deap = []

        if len(feas_univ_2) == 0:
            next_soln_set = []
        elif len(feas_univ_2) <= num_offspring:
            next_soln_set = feas_univ_2[:]
            feas_univ_2 = []
        else:
            #for index in range(num_offspring - 1, 0, -1):
            for index in reversed(range(0, num_offspring)):
                next_soln_set.append(feas_univ_2.pop(index))

        #print("\n length of next_soln_set: ", len(next_soln_set))
        #print(" length of feas_univ: ", len(feas_univ_2))

        return next_soln_set, feas_univ_2


    def getFeasSolnsUniv_Unique(self):
        feasible_solutions_list = []
        feasible_solutions_universe = []
        feasibleSolutions = json.loads(open("problems/workflow/answer_sidney_tokyo.json").read())
        cspData = json.loads(open("problems/workflow/csp_bigdata_db.json").read())

        for feasibleSolution in feasibleSolutions['answer']:
            feas_soln_res_format = []
            for tool_count in range(0, 4):
                tool = feasibleSolution['structure'][0]['compute'][tool_count]
                #print("index: ", tool_count)
                #print("toolname: ",tool['time'][0])
                #tool_1 = feasibleSolution['structure'][0]['compute'][0]
                tool_name = tool['time'][0]
                tool_vmtype = tool['instance_type']
                tool_vmregion = tool['location']
                feas_soln_res_format.append([tool_name, tool_vmtype, tool_vmregion])

            #print("\nfeas_soln_res_format: ", feas_soln_res_format)
            feasible_solutions_universe.append(feas_soln_res_format)
        
        
        feasible_solutions_universe_unique = []
        feasible_solutions_universe_unique_sorted = []
        feas_solns_list_unique =  []
        
        for item in feasible_solutions_universe:
        	if sorted(item) not in feasible_solutions_universe_unique_sorted:
        		feasible_solutions_universe_unique_sorted.append(sorted(item))
        		feasible_solutions_universe_unique.append(item)

        for feas_soln in feasible_solutions_universe_unique:
            feas_soln_int = [0 for _ in range(8)]
            for task_count in range(0, 4):
                for instanceType in cspData['aws']['instanceID']:
                    #print("\n uniq_feas_soln[task_count][1]: ", uniq_feas_soln[task_count][1])
                    if (instanceType['instanceType'] == feas_soln[task_count][1]):
                        #print("\n uniq_feas_soln: ", uniq_feas_soln)
                        #print("\n uniq_feas_soln[task_count][1]: ", uniq_feas_soln[task_count][1])
                        feas_soln_int[task_count] = instanceType['id']
                        for region in cspData['aws']['regionID']:
                            if (region['regionName'] == feas_soln[task_count][2]):
                                #print("\n uniq_feas_soln[task_count][2]: ", uniq_feas_soln[task_count][2])
                                feas_soln_int[task_count + 4] = region['id']
            feas_solns_list_unique.append(feas_soln_int)        
        
        
        feas_univ_unique = feas_solns_list_unique[:]

        return feas_univ_unique



    
    def split_list(self, the_list, chunk_size):
        result_list = []
        while the_list:
            result_list.append(the_list[:chunk_size])
            the_list = the_list[chunk_size:]
        
        return result_list
    
    
    def chromosomeToResource(self, chromosome):
        WFStruct = [['FastQC', [1, 5.0]], ['FastQC', [1, 5.0]], ['TopHat2', [2, 5.0]], ['Cufflinks', [1, 5.0]]] 
        
        vmTypeRegionSplit = self.split_list(chromosome, 4)
        cspData = json.loads(open("problems/workflow/csp_bigdata_db.json").read())

        
        wf_chromToRes_list = []
        
        task_count = 0
        
        for tool in WFStruct:
            tool_name = tool[0]
            vm_id = vmTypeRegionSplit[0][task_count]
            region_id = vmTypeRegionSplit[1][task_count]
            
            for instance in cspData['aws']['instanceID']:
                if instance['nodeType'] == 'cmpt' and instance['id'] == vm_id:
                    vm_type = instance['instanceType']
                    
            for region in cspData['aws']['regionID']:
                if region['id'] == region_id:
                    vm_region = region['regionName']
                    vm_location = region['locationName']
                    
            tool_chromo_to_resource = [tool_name, vm_type, vm_region]
            
            wf_chromToRes_list.append(tool_chromo_to_resource)
            
            task_count += 1
            
        return wf_chromToRes_list


    
    
    
    def removeDuplicates(self, pop):
        no_twins = []
        obj_values = []
        
        for indiv in pop:
        	'''
        	#if (indiv.objectives[0], indiv.objectives[1], indiv.objectives[2]) not in obj_values:
        		#obj_values.append((indiv.objectives[0], indiv.objectives[1], indiv.objectives[2]))
        		#no_twins.append(indiv)
        	'''

        	if (indiv.objectives[0],indiv.objectives[1]) not in obj_values:
        		obj_values.append((indiv.objectives[0],indiv.objectives[1]))
        		no_twins.append(indiv)
        		
        #print("\n no_twins : ", no_twins)
        
        return no_twins


    def removeDuplicates_basedon_NormOBjs(self, pop):
        no_twins = []
        normobj_refid_values = []
        
        for indiv in pop:
        	if (indiv.norm_objectives, indiv.reference_id) not in normobj_refid_values:
        		normobj_refid_values.append((indiv.norm_objectives, indiv.reference_id))
        		no_twins.append(indiv)
        		
        #print("\n no_twins : ", no_twins)
        
        return no_twins
        
    def removeDuplicates_basedon_RefID(self, pop):
     	no_twins = []
        refid_values = []
        
        for indiv in pop:
        	if indiv.reference_id not in refid_values:
        		refid_values.append(indiv.reference_id)
        		no_twins.append(indiv)
        		
        #print("\n no_twins : ", no_twins)
        
        return no_twins


    def removeDuplicates_basedon_RefID_perp_rank(self, pop):
     	#no_twins = []
     	attribs_list = []
     	
     	no_twins = copy.deepcopy(pop)
     	pop_rem = copy.deepcopy(pop)
     	
     	for ind_one in pop:
     		pop_rem.remove(ind_one)
     		for ind_two in pop_rem:
     			if (ind_one in no_twins) and (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank <= ind_one.rank):
     				no_twins.remove(ind_one) 
     				#break
     			elif (ind_two in no_twins) and (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank > ind_one.rank):
     				no_twins.remove(ind_two)
     				#break
     			elif (ind_two.reference_id == 4): 
     				print("\nFound one Ref 4 as ind_two: ", (ind_two.reference_id, ind_two.rank))
     				print(ind_two)
     		if (ind_one.reference_id == 4): 
     			print("\nFound one Ref 4 as ind_one: ", (ind_one.reference_id, ind_one.rank))
     			print(ind_one)
     				
     	     	
     	
     	
     	'''
     	for ind in pop:
     		if [ind.reference_id, ind.perpendicular, ind.rank] not in attribs_list:
     			attribs_list.append([ind.reference_id, ind.perpendicular, ind.rank])
     	print("\nattributes list: ", attribs_list)
     	
     	new_attribs_list = copy.deepcopy(attribs_list)
     	#print("\nNew attributes list Old_2: ", new_attribs_list)
     	
     	attribs_list_rem = copy.deepcopy(attribs_list)
     	
     	for att_list_one in attribs_list:
     		attribs_list_rem.remove(att_list_one)
     		for att_list_two in attribs_list_rem:
     			if (att_list_one in new_attribs_list) and (att_list_two[0] == att_list_one[0]) and (att_list_two[2] <= att_list_one[2]):
     				new_attribs_list.remove(att_list_one) 
     				break
     			elif (att_list_two in new_attribs_list) and (att_list_two[0] == att_list_one[0]) and (att_list_two[2] > att_list_one[2]):
     				new_attribs_list.remove(att_list_two)
     	print("\nNew attributes list: ", new_attribs_list)
     	
     	
     	for attribs in new_attribs_list:
     		for indiv in pop:
     			if (indiv.reference_id == attribs[0]) and (indiv.rank == attribs[2]):
     				no_twins.append(indiv)
     				break
     	
     	#print("\n No_twins ", no_twins)
     	'''
     	
     	'''
     	no_twins = copy.deepcopy(pop)
        
        
        intermed_pop = copy.deepcopy(pop)
        rem_pop = copy.deepcopy(pop)

        
        for ind_one in intermed_pop:
        	rem_pop.remove(ind_one)
        	#if len(rem_pop) > 0:
        	for ind_two in rem_pop:
        		if (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank <= ind_one.rank):
        			no_twins.remove(ind_one)
        			break
        		elif (ind_two.reference_id == ind_one.reference_id) and (ind_two.rank > ind_one.rank):
        			#print("\nno_twins", no_twins)
        			no_twins.remove(ind_two)
        		#else:
        		#	no_twins.append(ind_two)

        		
        #print("\n no_twins : ", no_twins)
        '''
        
        print("\nChecking attributes Ref_ID, Rank...")
        for chk_ind in no_twins:
        	print("Attributes: ", (chk_ind.reference_id, chk_ind.rank))
        return no_twins
    
    
    def normalizeFitnessValues(self, fitness_values):
		#fitness_values is a list of the form [runCost(in $), makespan(in secs), availability]
		# runCost range: 0-800 --> divide by 10
		# makespan range: 0-500,000 --> divide by 500,000
		# availability range: 0.9984-0.9996 --> minus 0.9980 then multiply by 100
		
		runCost_norm = fitness_values[0] / 10
		makespan_norm = fitness_values[1] / 500000
		availability_norm = (fitness_values[2] - 0.9980) * 0.5 * 1000
		#availability_norm = fitness_values[2]
		
		fitness_normalized = [runCost_norm, makespan_norm, availability_norm]
		
		return fitness_normalized


    def evaluate(self, chromosome):
        
        #print("\n chromosome:", chromosome)
        
        WFTaskList = self.split_list(chromosome, 4)
        
        #print("\n WFTaskList:", WFTaskList)
        
        #WFTaskList = [[1, 1, 7, 2], [5, 5, 3, 8]]
        
        WFStruct = [['FastQC', [1, 5.0]], ['FastQC', [1, 5.0]], ['TopHat2', [2, 5.0]], ['Cufflinks', [1, 5.0]]]
        
        WFStruct_Leveled = [[['FastQC', [1, 5.0]], ['FastQC', [1, 5.0]]], [['TopHat2', [2, 5.0]]], [['Cufflinks', [1, 5.0]]]]
        
        cspData = json.loads(open("problems/workflow/csp_bigdata_db.json").read())
        
        runningCostData = self.costDataCreator(WFTaskList, cspData) 
        runCost = self.runningCost(runningCostData, WFStruct, WFTaskList, cspData)
        #print("\n runningCostData",runningCostData)
        #print("\n runningCost", runCost)
        
        makespan = self.workflowMakespanSingle(WFStruct_Leveled, WFTaskList, cspData)
        #print("\n makespan", makespan)
        
        systAvailability = self.availabilityDataCreator(WFTaskList, cspData)
        #print("\n systAvailability", systAvailability)
        
        min_systAvailability = -1 * systAvailability
        
        round_runCost = round(runCost, 2)
        round_makespan = round(makespan, 2)
        round_systAvailability = round(systAvailability, 4)
        
        f = [runCost, makespan, systAvailability]

        
        f_norm = self.normalizeFitnessValues(f)
        
        #round_runCost = round(f_norm[0], 3)
        #round_makespan = round(f_norm[1], 3)
        #round_systAvailability = round(f_norm[2], 3)
        
        f_round = [round_runCost, round_makespan, round_systAvailability]
        
        return f_round


if __name__ == "__main__":
    print("Startup")
