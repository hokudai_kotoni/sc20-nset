from __future__ import print_function, division
import sys, os
sys.path.append(os.path.abspath("."))
import json

from utils.stat import Stat

# Problems
from problems.workflow.wf3 import WF3

# Optimizers
from algorithms.nset.nset import NSET


__author__ = 'kotoni'

#Paths to the various data sources
#wf_dp = sys.argv[2][:-1]
#csp_dp = sys.argv[3][:-1]
#feasibles_dp = sys.argv[4][:-1]
#metrics_dp = sys.argv[5][:-1]

#Desired number of solutions
#num_solns = sys.argv[6][:-1]


#wf_funcs = WF3(wf_dp, csp_dp, metrics_dp, feasibles_dp)
wf_funcs = WF3()


def attachObjValues(solns_list):
    solns_with_objs = []
    
    for soln in solns_list:
        objs_vals = wf_funcs.evaluate(soln)
        solns_with_objs.append([soln, objs_vals])
    
    return solns_with_objs



def findAllObjSolns(solns_with_objs, set_of_objs):
    # a solution with objective has the form [solution, objectives]
    
    found_solns = []
    
    for soln in solns_with_objs:
        if soln[1] == set_of_objs:
            found_solns.append(soln[0])
    
    return found_solns

#solns_list = wf_funcs.getFeasSolnsUniv_Unique()
#solns_with_objs = attachObjValues(solns_list)

#print(findAllObjSolns(solns_with_objs, [3.53, 20.09, 0.9984]))

#print("Generating final results list")
#print("\n[Running Cost, Makespan, Availability]")

# Read final population objectives data from file final_pop_objs_data.json
with open('output_data/intermediate_data/final_pop_objs_data.json', 'r') as f:
     final_pop_objs = json.load(f)

solns_list = wf_funcs.getFeasSolnsUniv_Unique()
solns_with_objs = attachObjValues(solns_list)

feas_optimal_configs = []

final_data = {}

final_data['job_name'] = []

set_index = 1

for obj_set in final_pop_objs:
    print("\n ==== Objective Set %s ====" %(set_index))
    print(obj_set)
    print("\n Corresponding Resource Configurations")
    assoc_solns = findAllObjSolns(solns_with_objs, obj_set)
    
    #final_data['solutions'].append({
    #    	'id': set_index,
    #    	'cost': obj_set[0],
    #    	'makespan': obj_set[1],
    #    	'availability': obj_set[2]})
    #print("\n ==== solns_with_objs %s ====" %(solns_with_objs))
    for soln in assoc_solns:
        soln_resources = wf_funcs.chromosomeToResource(soln)
        print(soln_resources)
        feas_optimal_configs.append([soln, soln_resources])
        
       
        final_data['job_name'].append({
        	'id': set_index,
        	'cost': obj_set[0],
        	'makespan': obj_set[1],
        	'availability': obj_set[2],
        	'structure': [
        		{'tool_name': soln_resources[0][0], 'instance_type': soln_resources[0][1],'location': soln_resources[0][2]},
        		{'tool_name': soln_resources[1][0], 'instance_type': soln_resources[1][1],'location': soln_resources[1][2]},
        		{'tool_name': soln_resources[2][0], 'instance_type': soln_resources[2][1],'location': soln_resources[2][2]}
        		]
        	})

        
    print("\n")
    set_index += 1

#print(final_data) 

# Write feasible optimal configs to file feas_optimal_configs_data.json
with open('output_data/feas_optimal_configs_data.json', 'w') as f:
     json.dump(final_data, f, indent=4)


os.system('python feas_opt_soln_selector.py')

# Write feasible optimal configs to file feas_optimal_configs_data.json
#with open('output_data/feas_optimal_configs_data.json', 'w') as f:
#     json.dump(feas_optimal_configs, f)